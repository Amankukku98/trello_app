import React, { Component } from 'react';
import * as TrelloApi from '../../api';
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
import SaveIcon from '@mui/icons-material/Save';
import DeleteIcon from '@mui/icons-material/Delete';
import {
    Box, Text, Button,
    Popover,
    PopoverTrigger,
    PopoverContent,
    PopoverBody,
    PopoverArrow,
    PopoverCloseButton,
    Input
} from '@chakra-ui/react';

class CardDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cards: [],
            hasError: false,
            cardName: '',
        }
    }


    createCard = () => {
        const listId = this.props;
        TrelloApi.createCard(this.props.listId, this.state.cardName)
            .then((res) => {
                const newCards = [...this.state.cards];
                newCards.push(res);
                this.setState({ cards: newCards, cardName: '' })
            })
    }
    handleDelete = (id) => {
        console.log("delete card");
        TrelloApi.deleteCard(id)
            .then((res) => {
                const newCards = this.state.cards.filter((card) => {
                    return card.id !== id;
                })
                this.setState({ cards: newCards });
            })
    }

    async componentDidMount() {
        const listId = this.props;
        await TrelloApi.getCard(this.props.listId)
            .then(card => {
                this.setState({
                    cards: card,
                    hasError: false,

                });
            }).catch((err) => {
                this.setState({
                    cards: [],
                    hasError: "Failed to load cards",
                });
            })

    }
    render() {
        const { cards } = this.state;
        return (
            <>
                {cards.map((card, index) => {
                    return <Box bg="white" mt={4} textAlign="center" key={index} display="flex" justifyContent="space-between">
                        <Text >{card.name}</Text><DeleteIcon onClick={() => { this.handleDelete(card.id) }} />
                    </Box>

                })}
                <Box>
                    <Popover placement="bottom" ml="8rem">
                        <PopoverTrigger>
                            <Box display="flex" justifyContent="space-between">
                                <Button mt={6} ml={4} bg="gray.300" color="gray.50" border="none"> + Add a card</Button> <Box mt={8} color="gray.50" mr={3}><SaveIcon /></Box>
                            </Box >
                        </PopoverTrigger>
                        <PopoverContent>
                            <PopoverArrow />
                            <PopoverBody><Input type="text" onChange={e => { this.setState({ cardName: e.target.value }) }} placeholder="Enter a title for this card..." /></PopoverBody>
                            <Box >
                                <PopoverBody><Button ml="5rem" onClick={this.createCard} bg="#026AA7" color="white">Add card</Button></PopoverBody>
                                <PopoverCloseButton mt="4.2rem" mr="5rem" fontSize={18} />
                            </Box>
                        </PopoverContent>
                    </Popover>
                </Box>

            </>

        )
    }
}
export default CardDetails;
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as TrelloApi from '../../api';
import {
    Box, Heading, Button,
    Popover,
    PopoverTrigger,
    PopoverContent,
    PopoverHeader,
    PopoverBody,
    PopoverArrow,
    PopoverCloseButton,
    Input
} from '@chakra-ui/react';
import { ariaHidden } from '@mui/material';


class Board extends Component {
    constructor(props) {
        super(props);
        this.state = {
            boards: [],
            isLoading: true,
            hasError: false,
            boardName: '',
            isOpen: false

        }
    }

    createBoard = () => {

        TrelloApi.createBoards(this.state.boardName)
            .then((res) => {
                const newBoards = [...this.state.boards];
                newBoards.push(res);
                this.setState({
                    boards: newBoards,
                    isOpen: false,
                })
            })
    }
    handleClick = () => {
        this.setState({ isOpen: true })
    }
    handleCloseClick = () => { this.setState({ isOpen: false }) }



    async componentDidMount() {
        await TrelloApi.getAllBoards()
            .then(boards => {
                this.setState({
                    boards: boards,
                    isLoading: false,
                    hasError: false,
                });
            })
            .catch((err) => {
                this.setState({
                    boards: [],
                    isLoading: false,
                    hasError: "Failed to load Boards",
                });
            })
    }


    render() {
        const { boards } = this.state;

        if (this.state.hasError) {
            return (
                <div>
                    <h1>{this.state.hasError}</h1>
                </div>
            )
        }

        if (this.state.isLoading) {
            return (
                <div>
                    <h1>Loading...</h1>
                </div>
            );
        }

        return (
            <Box display="flex" flexWrap="wrap">

                {
                    boards.map((board, index) => {
                        return (
                            <Link to={`/${board.id}`} key={index}>
                                <Box display="flex" flexWrap="wrap" >
                                    <Box onClick={this.handleBoard} backgroundImage="url('https://trello-backgrounds.s3.amazonaws.com/SharedBackground/original/49f33090455fdd208c1693adb9ecac7f/photo-1664737061963-862d6a174a3b')"
                                        backgroundPosition="center"
                                        backgroundRepeat="no-repeat"
                                        backgroundSize={300}
                                        h="10rem" w="20rem" ml="2rem" mt="5rem" color="white" textAlign="center" border="1px">
                                        <Heading as="h1">{board.name}</Heading>
                                    </Box >
                                </Box>
                            </Link>

                        )
                    })
                }
                <Box mt="5rem" ml="2rem">
                    <Popover placement='right'   >
                        <PopoverTrigger>
                            <Box
                                h="10rem" bg="skyblue" w="20rem" ml="2rem" color="white" textAlign="center" border="1px">
                                <Heading as="h1" mt="3.5rem" >Create new board</Heading>
                            </Box >
                        </PopoverTrigger>
                        <PopoverContent>
                            <PopoverArrow />
                            <PopoverCloseButton />
                            <PopoverHeader textAlign="center" fontSize={18} color="red">Create board</PopoverHeader>
                            <PopoverBody bg="#8EEBEC" p="2rem 4rem" mb={5}><img src="https://a.trellocdn.com/prgb/dist/images/board-preview-skeleton.14cda5dc635d1f13bc48.svg" alt="" role="presentation" /></PopoverBody>
                            <PopoverBody>Board Title</PopoverBody>
                            <PopoverBody><Input type="text" onChange={e => { this.setState({ boardName: e.target.value }) }} /></PopoverBody>
                            <PopoverBody mt={-3}>Board title is required</PopoverBody>
                            <PopoverBody><Button ml="7rem" onClick={this.createBoard}>Create</Button></PopoverBody>
                        </PopoverContent>
                    </Popover>
                </Box>


            </Box>
        )
    }
}

export default Board;
